import 'dart:async';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:firebase_database/firebase_database.dart';

import '../helper/weather_event.dart';
import '../helper/weather_state.dart';
import '../model/weather.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final _databaseReference = FirebaseDatabase.instance.ref().child('weather');

  WeatherBloc() : super(WeatherInitial());

  @override
  Stream<WeatherState> mapEventToState(WeatherEvent event) async* {
    yield WeatherLoading();
    try {
      final weather = await fetchWeather(event.city);

      final cityName = weather.city;
      // Save weather data to Firebase
      _databaseReference.child(cityName).set({
        'temperature': weather.temperature,
        'condition': weather.condition,
      });
      yield WeatherLoaded(weather);
    } catch (e) {
      yield WeatherError('Failed to load weather data');
    }
  }

  Future<Weather> fetchWeather(String city) async {
    const apiKey = '7b114153676f091855ed68761b553c29';
    final url =
        'http://api.openweathermap.org/data/2.5/weather?q=$city&appid=$apiKey&units=metric';
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      final jsonData = jsonDecode(response.body);
      return Weather.fromJson(jsonData);
    } else {
      throw Exception('Failed to load weather data');
    }
  }
}
