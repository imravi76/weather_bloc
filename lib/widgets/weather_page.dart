import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../data/weather_bloc.dart';
import '../helper/weather_event.dart';
import '../helper/weather_state.dart';

class WeatherPage extends StatelessWidget {
  final TextEditingController _cityController = TextEditingController();

  WeatherPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: _cityController,
              decoration: const InputDecoration(
                labelText: 'Enter City Name',
              ),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                final city = _cityController.text;
                if (city.isNotEmpty) {
                  context.read<WeatherBloc>().add(WeatherEvent(city));
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      content: Text('Please enter a city name.'),
                    ),
                  );
                }
              },
              child: const Text('Fetch Weather'),
            ),
            const SizedBox(height: 20),
            BlocBuilder<WeatherBloc, WeatherState>(
              builder: (context, state) {
                if (state is WeatherLoading) {
                  return const CircularProgressIndicator();
                } else if (state is WeatherLoaded) {
                  final weather = state.weather;
                  return Column(
                    children: [
                      Text(
                        'City: ${weather.city}',
                        style: const TextStyle(fontSize: 20),
                      ),
                      const SizedBox(height: 10),
                      Text(
                        'Temperature: ${weather.temperature}°C',
                        style: const TextStyle(fontSize: 20),
                      ),
                      const SizedBox(height: 10),
                      Text(
                        'Condition: ${weather.condition}',
                        style: const TextStyle(fontSize: 20),
                      ),
                    ],
                  );
                } else if (state is WeatherError) {
                  return Text('Error: ${state.message}');
                } else {
                  return const Text(
                      'Enter a city name and fetch weather data.');
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
