class City {
  final String name;

  City(this.name);

  factory City.fromJson(Map<String, dynamic> json) {
    return City(json['name']);
  }
}
