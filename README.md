# weather_bloc

A new Flutter project.

Instructions:
1. Clone this repo.
2. Install flutter sdk and android studio.
3. Open this project in android studio.
4. Attach your mobile or emulator.
5. Run the project.
6. flutter build apk for building the apk and distributing.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
